﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Media3D;
using System.IO;

using Autodesk.Revit.DB;
using Autodesk.Revit.DB.Architecture;
using Autodesk.Revit.UI;
using Autodesk.Revit.UI.Selection;
using Autodesk.Revit.ApplicationServices;
using Autodesk.Revit.Attributes;
using Autodesk.DesignScript.Geometry;
using Autodesk.DesignScript.Interfaces;
using Autodesk.DesignScript.Runtime;



namespace DeathRay
{
    [TransactionAttribute(TransactionMode.Manual)]
    [RegenerationAttribute(RegenerationOption.Manual)]
    public class MainCommand : IExternalCommand
    {
        public Result Execute(
          ExternalCommandData commandData,
          ref string message,          
          ElementSet elements)
        {

            //CullSunList(); // I just used this one time to actually cull the data. then I used the new file
            Days = new List<Day>();
            FillDays();
            Document doc = commandData.Application.ActiveUIDocument.Document;
            SetOptions();
            List<Wall> curtainWalls = GetVisibleCurtainPanels(doc);
            List<PanelData> panelsdata = GetPanels(curtainWalls, doc);
            Panels = panelsdata;
            SetReflectionVectors();
            //DrawLineReflections(commandData.Application);
            StoreLineReflections(commandData.Application); 
            return Result.Succeeded;
        }

        public Surface CreateSurface (List<XYZ> pts)
        {

            Autodesk.DesignScript.Geometry.Point p1 = Autodesk.DesignScript.Geometry.Point.ByCoordinates(pts[0].X, pts[0].Y, pts[0].Z);
            Autodesk.DesignScript.Geometry.Point p2 = Autodesk.DesignScript.Geometry.Point.ByCoordinates(pts[1].X, pts[1].Y, pts[1].Z);
            Autodesk.DesignScript.Geometry.Point p3 = Autodesk.DesignScript.Geometry.Point.ByCoordinates(pts[2].X, pts[2].Y, pts[2].Z);
            Autodesk.DesignScript.Geometry.Point p4 = Autodesk.DesignScript.Geometry.Point.ByCoordinates(pts[3].X, pts[3].Y, pts[3].Z);
            List<Autodesk.DesignScript.Geometry.Point> points = new List< Autodesk.DesignScript.Geometry.Point>();
            points.Add(p1);
            points.Add(p2);
            points.Add(p3);
            points.Add(p4);
            Autodesk.DesignScript.Geometry.Surface s = Surface.ByPerimeterPoints(points);
            return s;
        }

        public Geometry[] ProjectSurface(Surface accepter, Surface projecter)
        {
            List<Autodesk.DesignScript.Geometry.Vertex> vertexes = projecter.Vertices.ToList();
            List<Autodesk.DesignScript.Geometry.Point> upPoints = new List<Autodesk.DesignScript.Geometry.Point>();
            foreach(Vertex v in vertexes)
            {
                Autodesk.DesignScript.Geometry.Point p = v.PointGeometry;
                Autodesk.DesignScript.Geometry.Point upPoint = Autodesk.DesignScript.Geometry.Point.ByCoordinates(p.X, p.Y, (p.Z + 1));
                upPoints.Add(upPoint);
            }
            Autodesk.DesignScript.Geometry.Surface upSurface = Surface.ByPerimeterPoints(upPoints);
            Autodesk.DesignScript.Geometry.Vector down = Vector.ByCoordinates(0, 0, -1);
            Autodesk.DesignScript.Geometry.Geometry[] result = accepter.ProjectInputOnto(upSurface, down);
            //Autodesk.DesignScript.Geometry.Surface rresult = result.Cast<Surface>();
            if (result != null)
            {
                return result;
            }
            else
            {
                return null;
            }

        }

        public void DrawLineReflections(UIApplication app)
        {
            foreach (PanelData p in Panels)
            {
                if (p.Days.Count > 0)
                {
                    foreach (ElemDay day in p.Days)
                    {
                        if (day.TimeAngles.Count > 0)
                        {
                            foreach (TimeAngle t in day.TimeAngles)
                            {
                                //Autodesk.DesignScript.Geometry.Surface s = CreateSurface(t.ReflectionGeometry);
                                XYZ pt1 = t.ReflectionGeometry[0];
                                XYZ pt2 = t.ReflectionGeometry[1];
                                XYZ pt3 = t.ReflectionGeometry[2];
                                XYZ pt4 = t.ReflectionGeometry[3];
                                Transaction trans = new Transaction(app.ActiveUIDocument.Document, "deathray");
                                trans.Start();
                                CreateModelLine(pt1, pt2, app);
                                CreateModelLine(pt2, pt3, app);
                                CreateModelLine(pt3, pt4, app);
                                CreateModelLine(pt4, pt1, app);
                                trans.Commit();                                
                                
                            }
                        }
                    }
                }
            }
        }

        public void StoreLineReflections(UIApplication app)
        {
            foreach (PanelData p in Panels)
            {
                if (p.Days.Count > 0)
                {
                    foreach (ElemDay day in p.Days)
                    {
                        if (day.TimeAngles.Count > 0)
                        {
                            foreach (TimeAngle t in day.TimeAngles)
                            {
                                Autodesk.DesignScript.Geometry.Surface s = CreateSurface(t.ReflectionGeometry);
                                XYZ pt1 = t.ReflectionGeometry[0];
                                XYZ pt2 = t.ReflectionGeometry[1];
                                XYZ pt3 = t.ReflectionGeometry[2];
                                XYZ pt4 = t.ReflectionGeometry[3];
                                
                                t.ReflectionLines.Add(CreateLine(pt1, pt2, app));
                                t.ReflectionLines.Add(CreateLine(pt2, pt3, app));
                                t.ReflectionLines.Add(CreateLine(pt3, pt4, app));
                                t.ReflectionLines.Add(CreateLine(pt4, pt1, app));
                                
                                foreach (Day d in Days)
                                {
                                    if (d.Date == day.Date)
                                    {
                                        foreach (TimeAngle ta in d.TimeAngles)
                                        {
                                            if (ta.Time == t.Time)
                                            {
                                                LineSet ls = new LineSet();
                                                foreach (Autodesk.Revit.DB.Line l in t.ReflectionLines)
                                                {
                                                    ls.Lines.Add(l);                                                    
                                                }
                                                ta.AllReflections.Add(ls);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        public void CalcIntersects()
        {
            foreach (Day d in Days)
            {
                if (d.TimeAngles !=null)
                {
                    foreach (TimeAngle t in d.TimeAngles)
                    {
                        if (t != null)
                        {
                            for(int i 
                            {

                            }
                        }
                    }

                }
            }
        }

        public Autodesk.Revit.DB.Line CreateLine(XYZ p, XYZ q, UIApplication app)
        {            
            Autodesk.Revit.DB.Line line = Autodesk.Revit.DB.Line.CreateBound(p, q);
            return line;
        }

        public void CreateModelLine(XYZ p, XYZ q, UIApplication app)
        {            
            XYZ up = new XYZ(0, 0, 1);
            Autodesk.Revit.DB.Line line = Autodesk.Revit.DB.Line.CreateBound(p, q);
            Autodesk.Revit.DB.Plane plane = app.Application.Create.NewPlane(up, p);
            SketchPlane sketchPlane = SketchPlane.Create(app.ActiveUIDocument.Document, plane);
            app.ActiveUIDocument.Document.Create.NewModelCurve(line, sketchPlane);            
        }

        //public void AdjustForMullion()
        //{
        //    foreach (PanelData p in Panels)
        //    {
        //        foreach(XYZ corner in p.FaceCorners)
        //        {
        //            if ()
        //        }
        //    }
        //}

        public List<Wall> GetVisibleCurtainPanels (Document doc)
        {
            FilteredElementCollector fec = new FilteredElementCollector(doc);
            List<Wall> elems =fec.OfClass(typeof(Wall)).Cast<Wall>().ToList();
            List<Wall> curtainWalls = new List<Wall>();
            //foreach(Element e in elems)
            //{
            //    if (e.)
            //    {

            //        curtainWalls.Add(e);
            //    }

            //}
            foreach (Wall c in elems)
            {
                if (c.WallType.Kind == WallKind.Curtain)
                {
                    curtainWalls.Add(c);
                }
            }

            return curtainWalls;
        }

        public List<PanelData> GetPanels (List<Wall> curtainWalls, Document doc)
        {
            List<Panel> panels = new List<Panel>();
            List<PanelData> panelsData = new List<PanelData>();
            List<ElementId> panelIds = new List<ElementId>();
            List<PanelTransformMap> wallTs = new List<PanelTransformMap>();
            Transaction t = new Transaction(doc, "getting curtain grids");
            t.Start();
            foreach (Wall c in curtainWalls)
            {
                //Transform wallTrans = new Transform(null);
                GeometryElement wallGeo = c.get_Geometry(GlobalOptions);
                //foreach (GeometryObject go in wallGeo)
                //{
                //    GeometryInstance wallI = go as GeometryInstance;
                //    Transform wallT = wallI.Transform;
                //    wallTrans = wallT;
                //}
                foreach (ElementId id in c.CurtainGrid.GetPanelIds())
                {
                    if (panelIds.Contains(id) == false)
                    {
                        //PanelTransformMap m = new PanelTransformMap();
                        //m.ElemID = id;
                        //try
                        //{
                        //    foreach (GeometryObject go in wallGeo)
                        //    {
                        //        GeometryInstance wallI = go as GeometryInstance;
                                
                        //        //wallI.GetInstanceGeometry;
                        //        m.Transform = wallI.Transform;
                        //    }
                        //}
                        //catch { }
                        //wallTs.Add(m);
                        panelIds.Add(id);
                        panels.Add(doc.GetElement(id) as Panel);
                    }
                }
            
            }
            t.RollBack();

            int i = 0;
            // panel>geometryelement>geometryObject>Solid>Face
            foreach (Panel p in panels)
            {
                
                if (p != null)
                {
                    
                    PanelData pd = new PanelData();
                    pd.ElemID = panelIds[i];
                    i = i + 1;
                    LocationCurve lc = p.Location as LocationCurve;
                    GeometryElement geoElem = p.get_Geometry(GlobalOptions);
                    geoElem = geoElem.GetTransformed(Transform.Identity);
                    List<Autodesk.Revit.DB.Face> faces = new List<Autodesk.Revit.DB.Face>();
                    pd.Days = new List<ElemDay>();
                    foreach (ElemDay day in pd.Days)
                    {
                        day.TimeAngles = new List<TimeAngle>();
                    }
                    

                    foreach(GeometryObject g in geoElem)
                    {

                        //GeometryElement e = g.SymbolGeometry; //as GeometryElement;
                        //e.O
                        Autodesk.Revit.DB.Solid s = g as Autodesk.Revit.DB.Solid;
                        Autodesk.Revit.DB.Face face = g as Autodesk.Revit.DB.Face;

                        //GeometryInstance geoInstance = g as GeometryInstance;
                        ////Transform transform = geoInstance.Transform;
                        
                        //GeometryElement geoElement = geoInstance.GetSymbolGeometry();

                        //foreach(GeometryObject obj in geoElement)
                        //{
                        //    if (s == null)
                        //    {
                        //        s = obj as Autodesk.Revit.DB.Solid;
                                
                        //    }
                        //}
                        if (s != null)
                        {

                            Autodesk.Revit.DB.Face f = s.Faces.get_Item(0);                                                        
                            Autodesk.Revit.DB.UV uv = new Autodesk.Revit.DB.UV(.5, .5);
                            pd.Normal = f.ComputeNormal(uv);
                            Autodesk.Revit.DB.Mesh mesh = f.Triangulate();                            
                            List<XYZ> meshCorners = new List<XYZ>();
                            XYZ panelCenter = new XYZ();
                            foreach (XYZ point in mesh.Vertices)
                            {
                                //XYZ point2 = TransformPoint(point, transform);
                                //Transform wallTransform = new Transform(null);
                                //foreach(PanelTransformMap m in wallTs)
                                //{
                                //    if (m.ElemID == p.Id)
                                //    {
                                //        wallTransform = m.Transform;
                                //    }
                                //}
                                //XYZ point1 = point2;
                                //if (wallTransform != null)
                                //{
                                //    point1 = TransformPoint(point2, wallTransform);
                                //}
                                if (meshCorners.Contains(point) != true)
                                {
                                    meshCorners.Add(point);
                                    panelCenter = panelCenter + point;
                                }
                            }
                            pd.FaceCorners = meshCorners;
                            panelCenter = panelCenter / meshCorners.Count;
                            pd.Center = panelCenter;
                        }
                        if (face != null)
                        {
                            faces.Add(face);
                        }
                    }
                    if (pd != null)
                    {
                        panelsData.Add(pd);
                    }
                }
            }

           



            return panelsData;
        }
        public XYZ TransformPoint(XYZ point, Transform transform)
        {
            //revit store a transformation matrix for each return geoinstance. this calculation is to translate into real world coords
            double x = point.X;
            double y = point.Y;
            double z = point.Z;

            XYZ b0 = transform.get_Basis(0);
            XYZ b1 = transform.get_Basis(1);
            XYZ b2 = transform.get_Basis(2);
            XYZ origin = transform.Origin;

            double xTemp = x * b0.X + y * b1.X + z * b2.X + origin.X;
            double yTemp = x * b0.Y + y * b1.Y + z * b2.Y + origin.Y;
            double zTemp = x * b0.Z + y * b1.Z + z * b2.Z + origin.Z;

            XYZ tPoint = new XYZ(xTemp,yTemp,zTemp);

            return tPoint;
        }


        public void SetReflectionVectors()
        {
            //this class adds elemdays and reflection data to each panel
            for (int b = 0; b<Panels.Count;b++)
            {
                PanelData p = Panels[b]; // we can control which panel we want to work on and now we simulate one panel for multiple days or multiple times
                //this forloop controls which days we want to use for testing eventually it will be all for final test
                for (int i = 150; i<=150;i++)
                {
                    Day sunDay = Days[i];
                    ElemDay day = new ElemDay();
                    day.Date = sunDay.Date;
                    day.ElemID = p.ElemID;
                    day.TimeAngles = new List<TimeAngle>();
                    //this forloop controls which sun angle of that day we want to use for testing eventually it will be all
                    for (int a = 20; a<=23;a++)
                    {
                        TimeAngle st = sunDay.TimeAngles[a];
                        TimeAngle t = new TimeAngle();
                        t.Time = st.Time;
                        t.SunVector = st.SunVector;
                        t.ReflectionGeometry = new List<XYZ>();
                        XYZ reflection = ReflectionCalculator(p.Normal, t.SunVector);
                        XYZ reflectionIntersect = ReflectionIntesectionCalculator(p.Center, reflection);
                        foreach (XYZ pt in p.FaceCorners)
                        {
                            XYZ cornerIntersect = ReflectionIntesectionCalculator(pt, reflection);
                            t.ReflectionGeometry.Add(cornerIntersect);
                        }
                        //we can add reflection geometry calculations here
                        t.ReflectionVector = reflection;
                        t.ReflectionIntersect = reflectionIntersect;
                        day.TimeAngles.Add(t);
                    }
                    Panels[b].Days.Add(day);
                }              

            }
        }

        public XYZ ReflectionIntesectionCalculator(XYZ refOrigin, XYZ reflectionVector)
        {
            
            double heightFromPlane = refOrigin.Z - RoofElevation;
            double vectorMultiplier = heightFromPlane / reflectionVector.Z; //this is how many times we need to use vector to reach plane
            double xIntersect = refOrigin.X + (vectorMultiplier * reflectionVector.X); //origin plus how much we "drift" x/y in order to reach plane
            double yIntersect = refOrigin.Y + (vectorMultiplier * reflectionVector.Y);

            XYZ intersect = new XYZ(xIntersect,yIntersect,RoofElevation);
            return intersect;
        }

        public XYZ ReflectionCalculator(XYZ Normal, XYZ SunAngle)
        {
            //this calculates the reflection angle for a panel at a specific time of day/date
            Vector3D NormalVector = new Vector3D(Normal.X, Normal.Y, Normal.Z);
            Vector3D SolarVector = new Vector3D(SunAngle.X, SunAngle.Y, SunAngle.Z);
            //r=d-2(d.n)n is formula for calculating reflection
            Vector3D ReflectionVector = Vector3D.Subtract(SolarVector, Vector3D.Multiply(Vector3D.Multiply((Vector3D.DotProduct(SolarVector, NormalVector)), NormalVector), 2.0));

            XYZ Reflection = new XYZ(ReflectionVector.X, ReflectionVector.Y, ReflectionVector.Z);
            return Reflection;
        }

        public void FillDays ()
        {
            //fills the days list with accurate data to be used in later calculation
            StreamReader sr = new StreamReader(@"C:\\Users\brendannichols\Documents\Projects\MMMC DeathRay\Sun Angles8.txt");
            String s;
            Day day = new Day();
            while((s=sr.ReadLine())!= null)
            {
                string[] line = s.Split(',');
                List<string> strings = new List<string>();
                foreach (string str in line)
                {
                    strings.Add(str);
                }
                if (strings[0] != day.Date)
                {
                    if (day.Date != null)
                    {
                        Days.Add(day);
                    }
                    day = new Day();
                    day.Date = strings[0];
                }
                TimeAngle t = new TimeAngle();
                t.Time = strings[1];
                XYZ sunVector = new XYZ(Convert.ToDouble(strings[2]), Convert.ToDouble(strings[3]), Convert.ToDouble(strings[4]));
                t.SunVector = sunVector;
                if (day.TimeAngles == null)
                {
                    day.TimeAngles = new List<TimeAngle>();
                    day.TimeAngles.Add(t);
                }
                else
                {
                    day.TimeAngles.Add(t);
                }
            }
        }

        public class PanelData
        {
            public ElementId ElemID;
            public XYZ Center;
            public XYZ Normal;
            public List<XYZ> FaceCorners;
            public List<ElemDay> Days;
        }

        public class PanelTransformMap
        {
            public ElementId ElemID;
            public Transform Transform;
        }
        

        public static List<Day> Days { get; set; }

        public class ElemDay
        {
            public ElementId ElemID;
            public string Date;
            public List<TimeAngle> TimeAngles;
        }

        public class Day
        {
            public List<TimeAngle> TimeAngles;
            public string Date;
        }

        public class TimeAngle
        {
            public String Time;
            public XYZ SunVector;
            public XYZ ReflectionVector;
            public XYZ ReflectionIntersect;
            public List<XYZ> ReflectionGeometry;
            public List<Autodesk.Revit.DB.Line> ReflectionLines;
            public List<LineSet> AllReflections;
        }

        public class LineSet
        {
            public List<Autodesk.Revit.DB.Line> Lines { get; set; };
        }

        public void SetOptions ()
        {
            Options options
             = new Options();

            options.ComputeReferences = true;
            options.IncludeNonVisibleObjects = true;
            GlobalOptions = options;
        }
        public Options GlobalOptions { get; set; }

        public static double RoofElevation = -50;  //this is the number used to calculate the intersection of the reflections

        public static List<PanelData> Panels { get; set; }

        public void CullSunList()
        {
            StreamReader sr = new StreamReader(@"C:\\Users\brendannichols\Documents\Projects\MMMC DeathRay\Sun Angles7.txt");
            StreamWriter sw = new StreamWriter(@"C:\\Users\brendannichols\Documents\Projects\MMMC DeathRay\Sun Angles8.txt");
            String s;
            while ((s = sr.ReadLine())!= null)
            {
                string[] line = s.Split(',');
                List<string> strings = new List<string>();
                foreach (string str in line)
                {
                    strings.Add(str);
                }
                string MonthDay = strings[0];
                string[] mds = MonthDay.Split('/');
                List<string> md = new List<string>();
                foreach (string str in mds)
                {
                    md.Add(str);
                }
                int mon = new int();
                int day = new int();
                mon = Convert.ToInt32(md[0]);
                day = Convert.ToInt32(md[1]);

                
                string time = strings[1];
                string[] tme = time.Split(':');
                List<string> tmespl = new List<string>();
                foreach (string stme in tme)
                {
                    tmespl.Add(stme);
                }
                int hour = Convert.ToInt32(tmespl[0]);

                if(mon > 6 && mon <12)
                {
                    if(hour>6 && hour<20)
                    {
                        double altitude = (Convert.ToDouble(strings[2]) * Math.PI) / 180;
                        double azimuth = (Convert.ToDouble(strings[3]) * Math.PI) / 180;
                        double z = Math.Sin(altitude);
                        double hyp = Math.Cos(altitude);
                        double y = hyp * Math.Cos(azimuth);
                        double x = hyp * Math.Sin(azimuth);
                        sw.WriteLine(strings[0] + "," + strings[1] + "," + Convert.ToString(x) + "," + Convert.ToString(y) + "," + Convert.ToString(z));
                    }
                }
                else if (mon > 5 && day >20)
                {
                    if (hour > 6 && hour < 20)
                    {
                        double altitude = (Convert.ToDouble(strings[2])* Math.PI)/180;
                        double azimuth = (Convert.ToDouble(strings[3])*Math.PI)/180;
                        double z = Math.Sin(altitude);
                        double hyp = Math.Cos(altitude);
                        double y = hyp * Math.Cos(azimuth);
                        double x = hyp * Math.Sin(azimuth);
                        sw.WriteLine(strings[0] + "," + strings[1] + "," + Convert.ToString(x) + "," + Convert.ToString(y) + "," + Convert.ToString(z));
                    }
                }
                else if (mon == 12 && day <22)
                {
                    if (hour > 6 && hour < 20)
                    {
                        double altitude = (Convert.ToDouble(strings[2]) * Math.PI) / 180;
                        double azimuth = (Convert.ToDouble(strings[3]) * Math.PI) / 180;
                        double z = Math.Sin(altitude);
                        double hyp = Math.Cos(altitude);
                        double y = hyp * Math.Cos(azimuth);
                        double x = hyp * Math.Sin(azimuth);
                        sw.WriteLine(strings[0] + "," + strings[1] + "," + Convert.ToString(x) + "," + Convert.ToString(y) + "," + Convert.ToString(z));
                    }
                }

            }
        }
    }
}
